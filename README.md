# Where is the ISS?

This repository hosts a Snap! project that tracks the position of the ISS on a map. It uses the wheretheiss.at API to retrieve the latitude and longitude of the ISS as it orbits the planet. While this project could be used in a lesson, I often have it running in the background of classes to inspire students and get them interested in the advanced capabilities of Snap!

# Learning Objectives

SWBAT:

- Define the term API.
- Send requests to an API.
- Query responses from an API.
- Use move blocks to change the position of the ISS based on the response from the wheretheiss.at API.
- Use draw blocks to draw a path where the ISS has been over time.
- Create custom blocks that perform repeatable tasks.
- Add comments to their code to help them understand complex logic.
- Use the location settings in a web browser to plot their current position on the map.

**IMPORTANT**: To use this project in Snap!, you must first enable Javascript extensions in the editor.

